package com.dinhman.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Dinh Man on 29/02/2016.
 */

@RestController
public class HomeController {
    @RequestMapping(value = "/sayHello")
    public Object hello(@RequestParam("name") String name) {
        return "Hello";
    }
}
